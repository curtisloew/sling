package io.github.tslamic.sling.sling;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.util.Log;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;

import io.github.tslamic.sling.R;
import io.github.tslamic.sling.orm.AbstractPersister;

import static io.github.tslamic.sling.util.Util.throwExceptionIfMainThread;

/**
 * A {@link io.github.tslamic.sling.util.Persister} implementation,
 * saving stuff to a SQLite database.
 *
 * @see AbstractPersister
 * @see io.github.tslamic.sling.util.Persister
 */
public class SlingEntryPersister extends AbstractPersister<List<SlingEntry>, Void> {

    private static final String KEY_LAST_HASHCODE = "SlingEntryPersister.KEY_LAST_HASHCODE";

    private static final String DATABASE_NAME = "sling.db";
    private static final int DATABASE_VERSION = 1;

    // This value helps us determine if we should save the SlingEntry list to the database.
    // It represents the last saved List<SlingEntry> hashcode. If a new list has the
    // same hash (and is therefore equal), no saving is done.
    // This is, of course, just a dummy example how some operations might be avoided if they
    // are unnecessary.
    private int mLastPersistentHash;

    public SlingEntryPersister(Context context) {
        super(context, DATABASE_NAME, DATABASE_VERSION, R.raw.ormlite_config);
        mLastPersistentHash = PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(KEY_LAST_HASHCODE, 0);
    }

    @Override
    protected void onCreateDb(SQLiteDatabase database,
                              ConnectionSource connectionSource) throws SQLException {
        TableUtils.createTable(connectionSource, SlingEntry.class);
    }

    @Override
    protected void onUpgradeDb(SQLiteDatabase database,
                               ConnectionSource connectionSource,
                               int oldVersion, int newVersion) throws SQLException {
        TableUtils.dropTable(connectionSource, SlingEntry.class, true);
        onCreate(database, connectionSource);
    }

    @Override
    public boolean save(List<SlingEntry> object) {
        throwExceptionIfMainThread();

        final RuntimeExceptionDao<SlingEntry, Integer> dao =
                getRuntimeExceptionDao(SlingEntry.class);
        for (SlingEntry entry : object) {
            dao.create(entry);
        }
        setLatestHash(object.hashCode());

        return true;
    }

    @Override
    public boolean update(List<SlingEntry> object) {
        throwExceptionIfMainThread();

        final RuntimeExceptionDao<SlingEntry, Integer> dao =
                getRuntimeExceptionDao(SlingEntry.class);
        for (SlingEntry entry : object) {
            dao.update(entry);
        }
        setLatestHash(dao.queryForAll().hashCode());

        return false;
    }

    @Override
    public List<SlingEntry> load(Void key) {
        throwExceptionIfMainThread();

        final RuntimeExceptionDao<SlingEntry, Integer> dao =
                getRuntimeExceptionDao(SlingEntry.class);
        return dao.queryForAll();
    }

    @Override
    public boolean shouldSave(List<SlingEntry> object) {
        return mLastPersistentHash != object.hashCode();
    }

    @Override
    public void outputStatistics() {
        throwExceptionIfMainThread();

        try {
            final StringBuilder builder = new StringBuilder();

            builder.append("--- SLING STATISTICS ---\n");
            builder.append("Since the loaded Bitmaps are resized by Picasso before " +
                    "actually loaded into memory, real sizes might differ.\n");
            builder.append("USERNAME - POSTS - AVG IMG SIZE - MAX WIDTH\n");

            final String separator = " - ";

            final RuntimeExceptionDao<SlingEntry, Integer> dao =
                    getRuntimeExceptionDao(SlingEntry.class);

            // The following could ultimately be a very long raw SQL query.

            final List<SlingEntry> entries = dao.queryBuilder()
                    .distinct()
                    .selectColumns("username")
                    .query();

            for (SlingEntry entry : entries) {
                final String username = entry.getUsername();
                final List<SlingEntry> usernameEntries = dao.queryBuilder()
                        .where()
                        .eq("username", username)
                        .query();

                final int allPostsByUsername = usernameEntries.size();

                long avgSize = 0L;
                long maxWidth = 0;
                for (SlingEntry se : usernameEntries) {
                    avgSize += se.getImageSize();

                    final int w = se.getWidth();
                    if (w > maxWidth) {
                        maxWidth = w;
                    }
                }

                avgSize /= allPostsByUsername;

                builder.append(username);
                builder.append(separator);
                builder.append(allPostsByUsername);
                builder.append(separator);
                builder.append(NumberFormat.getInstance().format(avgSize));
                builder.append(separator);
                builder.append(maxWidth);
                builder.append('\n');
            }
            Log.d("STAT", builder.toString());
        } catch (SQLException e) {
            Log.e("STAT", e.toString());
        }
    }

    private void setLatestHash(int hash) {
        mLastPersistentHash = hash;
        PreferenceManager.getDefaultSharedPreferences(mContext)
                .edit()
                .putInt(KEY_LAST_HASHCODE, mLastPersistentHash)
                .commit();
    }

}
