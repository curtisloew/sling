package io.github.tslamic.sling.util;

public interface PersistStrategy<T> {

    /**
     * Determines if an object should be saved.
     *
     * @param object the entity to be saved.
     * @return {@code true} if an object should be saved, {@code false} otherwise.
     */
    boolean shouldSave(T object);

    /**
     * Determines if an object should be loaded.
     *
     * @return {@code true} if an object should be loaded, {@code false} otherwise.
     */
    boolean shouldLoad();

}
