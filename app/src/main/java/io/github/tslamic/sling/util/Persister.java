package io.github.tslamic.sling.util;

public interface Persister<T, K> extends PersistStrategy<T> {

    /**
     * Saves an object.
     *
     * @param object object to be saved.
     * @return {@code true} if successfully saved, {@code false} otherwise.
     */
    boolean save(T object);

    /**
     * Updates an object.
     *
     * @param object object to be updated.
     * @return {@code true} if successfully updated, {@code false} otherwise.
     */
    boolean update(T object);

    /**
     * Loads an object.
     *
     * @param key the key to help identify which object to load.
     * @return the loaded object.
     */
    T load(K key);

    /**
     * Closes this {@code Persister}.
     */
    void close();

    /**
     * Outputs the object statistics.
     */
    void outputStatistics();

}
