package io.github.tslamic.sling.sling;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

import io.github.tslamic.sling.R;
import io.github.tslamic.sling.ui.SlingImageView;

public class SlingAdapter extends BaseAdapter {

    private final Context mContext;
    private final List<SlingEntry> mEntries;
    private final Picasso mPicasso;
    private final int mImageHeight;
    private final Sling mSling;

    public SlingAdapter(Context context, List<SlingEntry> entries, Sling sling) {
        mContext = context;
        mEntries = new ArrayList<>(entries);
        mPicasso = Picasso.with(context);
        mImageHeight = context.getResources().getDimensionPixelOffset(R.dimen.sling_entry_height);
        mSling = sling;
    }

    @Override
    public int getCount() {
        return mEntries.size();
    }

    @Override
    public Object getItem(int position) {
        return mEntries.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (null == convertView) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(R.layout.sling_entry, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final SlingEntry entry = mEntries.get(position);

        holder.prepareForAnimation();
        holder.image.setTag(holder.text);
        mPicasso.load(entry.getImageUrl())
                .transform(new SlingTransformation(entry))
                .resize(0, mImageHeight)
                .into((Target) holder.image);
        holder.text.setText(entry.getTitle());

        return convertView;
    }

    private static class ViewHolder {

        final SlingImageView image;
        final TextView text;

        public ViewHolder(View view) {
            image = (SlingImageView) view.findViewById(R.id.sling_image);
            text = (TextView) view.findViewById(R.id.sling_text);
        }

        private void prepareForAnimation() {
            text.setBackgroundColor(SlingImageView.BEFORE_ANIM_TEXT_BACKGROUND);
            text.setTextColor(SlingImageView.BEFORE_ANIM_TEXT_COLOR);
        }

    }

    private class SlingTransformation implements Transformation {

        private final SlingEntry mEntry;

        public SlingTransformation(SlingEntry entry) {
            mEntry = entry;
        }

        @Override
        public Bitmap transform(final Bitmap source) {
            // The if check represents a dummy logic for avoiding redundant work.
            if (mEntry.getHeight() == 0 || mEntry.getWidth() == 0) {
                mSling.getExecutorService().execute(new Runnable() {
                    @Override
                    public void run() {
                        mEntry.setWidth(source.getWidth());
                        mEntry.setHeight(source.getHeight());
                        final List<SlingEntry> list = new ArrayList<>(1);
                        list.add(mEntry);
                        mSling.update(list);
                    }
                });
            }
            return source;
        }

        @Override
        public String key() {
            return "SlingTransformation";
        }

    }

}
