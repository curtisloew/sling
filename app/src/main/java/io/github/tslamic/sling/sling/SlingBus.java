package io.github.tslamic.sling.sling;

import com.squareup.otto.Bus;

import io.github.tslamic.sling.util.Event;
import io.github.tslamic.sling.util.EventBus;

public enum SlingBus implements EventBus {

    INSTANCE;

    private final Bus mBus = new Bus();

    @Override
    public void register(Object object) {
        mBus.register(object);

    }

    @Override
    public void unregister(Object object) {
        mBus.unregister(object);

    }

    @Override
    public void post(Event event) {
        mBus.post(event);
    }

}
