package io.github.tslamic.sling.util;

import android.os.Looper;

public final class Util {

    public static <T> T checkNotNull(T object, String message) {
        if (null == object) {
            throw new NullPointerException(message);
        }
        return object;
    }

    public static void throwExceptionIfMainThread() {
        if (isOnMainThread()) {
            throw new IllegalStateException("running on main thread");
        }
    }

    public static void throwExceptionIfWorkerThread() {
        if (isNotOnMainThread()) {
            throw new IllegalStateException("running on worker thread");
        }
    }

    public static boolean isOnMainThread() {
        return Looper.getMainLooper().getThread() == Thread.currentThread();
    }

    public static boolean isNotOnMainThread() {
        return !isOnMainThread();
    }

    private Util() {
    }

}
