package io.github.tslamic.sling.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.squareup.otto.Subscribe;

import java.util.List;

import io.github.tslamic.sling.R;
import io.github.tslamic.sling.event.ErrorEvent;
import io.github.tslamic.sling.event.FeedEvent;
import io.github.tslamic.sling.event.PendingEvent;
import io.github.tslamic.sling.sling.Sling;
import io.github.tslamic.sling.sling.SlingAdapter;
import io.github.tslamic.sling.sling.SlingEntry;

public class FeedFragment extends Fragment {

    public static final String TAG = "FeedFragment.TAG";

    private SlingAdapter mAdapter;
    private ProgressBar mProgress;
    private ListView mList;
    private Sling mSling;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mSling = new Sling(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.feed, container, false);
        final FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.feed_more_data);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSling.outputStatistics();
                Snackbar.make(v, R.string.statistics, Snackbar.LENGTH_SHORT).show();
            }
        });
        mProgress = (ProgressBar) view.findViewById(R.id.feed_progress);
        mList = (ListView) view.findViewById(R.id.feed_list);
        mList.setOnItemClickListener(new RotateItemClickListener());
        if (null != mAdapter) {
            mList.setAdapter(mAdapter);
        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mSling.register(this);
        mSling.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mSling.unregister(this);
        mSling.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSling.onDestroy();
    }

    @SuppressWarnings("unused") // Used by the event bus.
    @Subscribe
    public void onFeedEvent(FeedEvent event) {
        final Context context = getActivity();
        if (null != context) {
            final int firstVisible = mList.getFirstVisiblePosition();
            final List<SlingEntry> entries = event.getObject();
            mAdapter = new SlingAdapter(getActivity(), entries, mSling);
            mList.setAdapter(mAdapter);
            restoreListPosition(firstVisible);
        }
        showProgress(false);
    }

    @SuppressWarnings("unused") // Used by the event bus.
    @Subscribe
    public void onErrorEvent(ErrorEvent event) {
        final View view = getView();
        if (null != view) {
            Snackbar.make(view, R.string.cannot_access_feed, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mSling.downloadFeed();
                        }
                    })
                    .show();
        }
    }

    @SuppressWarnings("unused") // Used by the event bus.
    @Subscribe
    public void onPendingEvent(PendingEvent event) {
        showProgress(true);
    }

    private void restoreListPosition(int firstVisiblePosition) {
        final View v = mList.getChildAt(0);
        final int top = (null == v) ? 0 : (v.getTop() - mList.getPaddingTop());
        mList.setSelectionFromTop(firstVisiblePosition, top);
    }

    private void showProgress(boolean show) {
        mProgress.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private static class RotateItemClickListener implements AdapterView.OnItemClickListener {

        private static final int SCALE_DURATION = 200;
        private static final int ROTATE_DURATION = 500;

        private static final int STATE_ROTATE = 0;
        private static final int STATE_SCALE = 1;
        private static final int STATE_END = 2;

        @Override
        public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
            // A bit of trickery to avoid two other AnimatorListenerAdapters.
            final AnimatorListenerAdapter listener = new AnimatorListenerAdapter() {

                int mType = STATE_ROTATE;

                @Override
                public void onAnimationEnd(Animator animation) {
                    switch (mType) {
                        case STATE_ROTATE:
                            mType = STATE_SCALE;
                            view.animate()
                                    .rotationX(360f)
                                    .setDuration(ROTATE_DURATION)
                                    .setListener(this);
                            break;
                        case STATE_SCALE:
                            mType = STATE_END;
                            view.animate()
                                    .scaleX(1f)
                                    .scaleY(1f)
                                    .setDuration(SCALE_DURATION)
                                    .setListener(this);
                            break;
                        case STATE_END:
                            view.setScaleX(1f);
                            view.setScaleY(1f);
                            view.setRotationX(0f);
                            break;
                        default:
                            break;
                    }
                }
            };

            // The domino effect starts here.
            view.animate()
                    .scaleX(.5f)
                    .scaleY(.5f)
                    .setDuration(SCALE_DURATION)
                    .setListener(listener);
        }

    }

}
