package io.github.tslamic.sling.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import io.github.tslamic.sling.R;

public class SplashActivity extends Activity {

    private static final int DELAY = 1500;

    private final Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                launch(FeedActivity.class);
            }
        }, DELAY);
    }

    private void launch(Class<? extends Activity> clazz) {
        final Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }

}
