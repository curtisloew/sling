package io.github.tslamic.sling.event;

import java.util.List;

import io.github.tslamic.sling.sling.SlingEntry;

public class FeedEvent extends SimpleEvent<List<SlingEntry>> {

    public FeedEvent(List<SlingEntry> object) {
        super(object);
    }

}
