package io.github.tslamic.sling.activity;

import android.app.Activity;
import android.app.FragmentManager;
import android.os.Bundle;

import io.github.tslamic.sling.fragment.FeedFragment;

public class FeedActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final FragmentManager manager = getFragmentManager();
        if (null == manager.findFragmentByTag(FeedFragment.TAG)) {
            final FeedFragment f = new FeedFragment();
            manager.beginTransaction()
                    .replace(android.R.id.content, f, FeedFragment.TAG)
                    .commit();
        }
    }

}
