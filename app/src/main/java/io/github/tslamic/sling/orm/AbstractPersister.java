package io.github.tslamic.sling.orm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

import io.github.tslamic.sling.util.Persister;

import static io.github.tslamic.sling.util.Util.checkNotNull;

/**
 * A {@link Persister} stub.
 */
public abstract class AbstractPersister<T, K>
        extends OrmLiteSqliteOpenHelper implements Persister<T, K> {

    protected final Context mContext;

    public AbstractPersister(Context context, String databaseName,
                             int databaseVersion, int rawConfigResource) {
        super(context, databaseName, null, databaseVersion, rawConfigResource);
        checkNotNull(context, "context is null");
        mContext = context.getApplicationContext();
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            onCreateDb(database, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            onUpgradeDb(database, connectionSource, oldVersion, newVersion);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean shouldSave(T object) {
        return false;
    }

    @Override
    public boolean shouldLoad() {
        return false;
    }

    protected abstract void onCreateDb(SQLiteDatabase database,
                                       ConnectionSource connectionSource) throws SQLException;

    protected abstract void onUpgradeDb(SQLiteDatabase database,
                                        ConnectionSource connectionSource,
                                        int oldVersion, int newVersion) throws SQLException;

}
