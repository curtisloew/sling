package io.github.tslamic.sling.sling;

import android.content.Context;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.github.tslamic.sling.event.PendingEvent;
import io.github.tslamic.sling.util.Downloader;
import io.github.tslamic.sling.util.Event;
import io.github.tslamic.sling.util.EventBus;
import io.github.tslamic.sling.util.Persister;

public class Sling implements Downloader, EventBus, Persister<List<SlingEntry>, Void> {

    private final ExecutorService mExecutorService;
    private final SlingDownloader mDownloader;
    private final EventBus mEventBus;
    private final Persister<List<SlingEntry>, Void> mPersister;

    public Sling(Context context) {
        mExecutorService = Executors.newSingleThreadExecutor();
        mEventBus = SlingBus.INSTANCE;
        mPersister = new SlingEntryPersister(context);
        mDownloader = new SlingDownloader.Builder()
                .executor(mExecutorService)
                .eventBus(mEventBus)
                .persister(mPersister)
                .build();
    }

    public void onResume() {
        if (shouldDownload()) {
            downloadFeed();
        }
    }

    public void onPause() {
        // Do nothing.
    }

    public void onDestroy() {
        mPersister.close();
        mExecutorService.shutdown();
    }

    public ExecutorService getExecutorService() {
        return mExecutorService;
    }

    @Override
    public void downloadFeed() {
        mEventBus.post(PendingEvent.INSTANCE);
        mDownloader.downloadFeed();
    }

    @Override
    public void close() {
        mDownloader.close(); // No need to invoke it for this instance.
    }

    @Override
    public boolean shouldDownload() {
        return mDownloader.shouldDownload();
    }

    @Override
    public void register(Object object) {
        mEventBus.register(object);
    }

    @Override
    public void unregister(Object object) {
        mEventBus.unregister(object);
    }

    @Override
    public void post(Event event) {
        mEventBus.post(event);
    }

    @Override
    public boolean save(List<SlingEntry> object) {
        return mPersister.save(object);
    }

    @Override
    public boolean update(List<SlingEntry> object) {
        return mPersister.update(object);
    }

    @Override
    public List<SlingEntry> load(Void key) {
        return mPersister.load(key);
    }

    @Override
    public void outputStatistics() {
        mExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                mPersister.outputStatistics();
            }
        });
    }

    @Override
    public boolean shouldSave(List<SlingEntry> object) {
        return mPersister.shouldSave(object);
    }

    @Override
    public boolean shouldLoad() {
        return mPersister.shouldLoad();
    }

}
