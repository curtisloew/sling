package io.github.tslamic.sling.sling;

import java.util.List;

import retrofit.Call;
import retrofit.http.GET;

public interface SlingService {

    /**
     * Endpoint we're connecting to.
     */
    String BASE_URL = "http://challenge.superfling.com";

    /**
     * Pictures location.
     */
    String PIC_URL = "http://challenge.superfling.com/photos/%d";

    /**
     * Returns a {@link Call} capable of retrieving the Sling feed.
     */
    @GET("/")
    Call<List<SlingEntry>> getFeed();

}
