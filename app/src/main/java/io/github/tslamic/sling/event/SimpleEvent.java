package io.github.tslamic.sling.event;

import io.github.tslamic.sling.util.Event;

/**
 * Simple {@link Event} to be used with the {@link io.github.tslamic.sling.util.EventBus}.
 *
 * @param <T> the object type this {@code SimpleEvent} represents.
 */
class SimpleEvent<T> implements Event<T> {

    private final T mObject;

    public SimpleEvent(T object) {
        mObject = object;
    }

    @Override
    public T getObject() {
        return mObject;
    }

}
