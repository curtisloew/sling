package io.github.tslamic.sling.event;

import io.github.tslamic.sling.util.Event;

public enum PendingEvent implements Event<Void> {

    INSTANCE;

    @Override
    public Void getObject() {
        return null;
    }

}
