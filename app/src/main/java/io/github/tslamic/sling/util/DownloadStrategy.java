package io.github.tslamic.sling.util;

public interface DownloadStrategy {

    /**
     * Determines if a new download should start.
     *
     * @return {@code true} if a new download should start, {@code false} otherwise.
     */
    boolean shouldDownload();

}
