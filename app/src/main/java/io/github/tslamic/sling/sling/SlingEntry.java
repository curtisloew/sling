package io.github.tslamic.sling.sling;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "entries")
public class SlingEntry {

    @DatabaseField(id = true)
    @SerializedName("ID")
    private String id;

    @DatabaseField
    @SerializedName("ImageID")
    private int imageId;

    @DatabaseField
    @SerializedName("Title")
    private String title;

    @DatabaseField
    @SerializedName("UserID")
    private int userId;

    @DatabaseField
    @SerializedName("UserName")
    private String username;

    @DatabaseField
    private int width;

    @DatabaseField
    private int height;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageUrl() {
        return String.format(SlingService.PIC_URL, imageId);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public long getImageSize() {
        return width * height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final SlingEntry entry = (SlingEntry) o;

        if (imageId != entry.imageId) {
            return false;
        }
        if (userId != entry.userId) {
            return false;
        }
        return !(id != null ? !id.equals(entry.id) : entry.id != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + imageId;
        result = 31 * result + userId;
        return result;
    }

}
