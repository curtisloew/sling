package io.github.tslamic.sling.util;

public interface Downloader extends DownloadStrategy {

    /**
     * Triggers the feed download.
     *
     * It's up to the implementer to decide how the result is propagated.
     */
    void downloadFeed();

    /**
     * Closes this {@code Downloader}.
     */
    void close();

}
