package io.github.tslamic.sling.event;

public class ErrorEvent extends SimpleEvent<Integer> {

    public ErrorEvent(Integer object) {
        super(object);
    }

}
