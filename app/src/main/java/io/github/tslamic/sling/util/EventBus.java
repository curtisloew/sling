package io.github.tslamic.sling.util;

public interface EventBus {

    /**
     * Registers an object to receive this {@code EventBus} events.
     *
     * @param object {@link Object} to register.
     */
    void register(Object object);

    /**
     * Unregisters an object from receiving this {@code EventBus} events.
     *
     * @param object {@link Object} to register.
     */
    void unregister(Object object);

    /**
     * Posts a new event that will be propagated through this {@code EventBus}.
     *
     * @param event {@link Event} to be posted.
     */
    void post(Event event);

}
