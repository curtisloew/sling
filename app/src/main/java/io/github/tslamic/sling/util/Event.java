package io.github.tslamic.sling.util;

/**
 * A marker interface denoting an object propagated through an event bus.
 */
public interface Event<T> {

    /**
     * Returns the object associated with this event.
     *
     * @return the object associated with this event.
     */
    T getObject();

}
