package io.github.tslamic.sling.sling;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.github.tslamic.sling.event.ErrorEvent;
import io.github.tslamic.sling.event.FeedEvent;
import io.github.tslamic.sling.util.Downloader;
import io.github.tslamic.sling.util.Event;
import io.github.tslamic.sling.util.EventBus;
import io.github.tslamic.sling.util.Persister;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

import static io.github.tslamic.sling.util.Util.checkNotNull;
import static io.github.tslamic.sling.util.Util.throwExceptionIfMainThread;

/**
 * A simple {@link Downloader} implementation.
 */
public class SlingDownloader implements Downloader {

    public static final int ERROR_BAD_RESPONSE = 0;
    public static final int ERROR_BAD_REQUEST = 1;

    public static class Builder {

        private SlingService mService;
        private EventBus mBus;
        private Persister<List<SlingEntry>, Void> mPersister;
        private ExecutorService mExecutorService;
        private Handler mMainThreadHandler;

        public Builder service(SlingService service) {
            mService = checkNotNull(service, "service is null");
            return this;
        }

        public Builder eventBus(EventBus bus) {
            mBus = checkNotNull(bus, "event bus is null");
            return this;
        }

        public Builder persister(Persister<List<SlingEntry>, Void> persister) {
            mPersister = checkNotNull(persister, "persister is null");
            return this;
        }

        public Builder executor(ExecutorService service) {
            mExecutorService = checkNotNull(service, "service is null");
            return this;
        }

        public Builder handler(Handler handler) {
            mMainThreadHandler = checkNotNull(handler, "handler is null");
            if (!handler.getLooper().equals(Looper.getMainLooper())) {
                throw new IllegalArgumentException("handler does not have a main thread looper");
            }
            return this;
        }

        public SlingDownloader build() {
            if (null == mPersister) {
                throw new IllegalStateException("persister must be provided");
            }
            if (null == mService) {
                mService = getDefaultSlingService();
            }
            if (null == mBus) {
                mBus = SlingBus.INSTANCE;
            }
            if (null == mExecutorService) {
                mExecutorService = Executors.newSingleThreadExecutor();
            }
            if (null == mMainThreadHandler) {
                mMainThreadHandler = new Handler(Looper.getMainLooper());
            }
            return new SlingDownloader(this);
        }

    }

    private final SlingService mService;
    private final EventBus mBus;
    private final Persister<List<SlingEntry>, Void> mPersister;
    private final ExecutorService mExecutorService;
    private final Handler mMainThreadHandler;

    // This is just an example of how we might save the number of downloads.
    // For example, let's only do it every minute or so.
    private static final int ONE_MINUTE = 1000 * 60;
    private long mLastDownloadTime;

    private SlingDownloader(Builder builder) {
        mService = builder.mService;
        mBus = builder.mBus;
        mPersister = builder.mPersister;
        mExecutorService = builder.mExecutorService;
        mMainThreadHandler = builder.mMainThreadHandler;
    }

    @Override
    public void downloadFeed() {
        mExecutorService.execute(new DownloadRunnable());
    }

    @Override
    public void close() {
        mExecutorService.shutdown();
    }

    @Override
    public boolean shouldDownload() {
        final long now = SystemClock.elapsedRealtime();
        return now - mLastDownloadTime > ONE_MINUTE;
    }

    private static SlingService getDefaultSlingService() {
        return new Retrofit.Builder()
                .baseUrl(SlingService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(SlingService.class);
    }

    /**
     * Downloads the feed and saves it, all while on the worker thread.
     */
    private class DownloadRunnable implements Runnable {

        @Override
        public void run() {
            throwExceptionIfMainThread();
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

            final Event event;

            // Determine, based on the strategy, if we should download the feed or do
            // something else. As mentioned above, this implementation does the download
            // if more than a minute passed from the last download, otherwise the data from
            // local storage is used.
            if (shouldDownload()) {
                event = download();
            } else {
                // We create the same event even when the data comes from the persister.
                // Maybe in a real world scenario, this would not be the case.
                final List<SlingEntry> entries = mPersister.load(null);
                event = new FeedEvent(entries);
            }

            // Post results as an event bus event and ensure they are run on the main thread.
            mMainThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    mBus.post(event);
                }
            });
        }

        private Event download() {
            final Event event;

            final Response<List<SlingEntry>> response = exec();
            if (null == response) {
                event = new ErrorEvent(ERROR_BAD_REQUEST);
            } else if (response.isSuccess()) {
                final List<SlingEntry> entries = response.body();
                if (mPersister.shouldSave(entries)) {
                    mPersister.save(entries);
                }
                event = new FeedEvent(entries);
            } else {
                event = new ErrorEvent(ERROR_BAD_RESPONSE);
            }

            mLastDownloadTime = SystemClock.elapsedRealtime();
            return event;
        }

        private Response<List<SlingEntry>> exec() {
            Response<List<SlingEntry>> response = null;
            try {
                response = mService.getFeed().execute();
            } catch (IOException ignore) {
            }
            return response;
        }

    }

}
