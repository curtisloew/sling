package io.github.tslamic.sling.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * A subclass of {@link ImageView} capable of doing fancy animation when Picasso tells us to.
 */
public class SlingImageView extends ImageView implements Target {

    public static final int BEFORE_ANIM_TEXT_COLOR = 0xFF303F9F;
    public static final int AFTER_ANIM_TEXT_COLOR = Color.WHITE;
    public static final int BEFORE_ANIM_TEXT_BACKGROUND = Color.WHITE;
    public static final int AFTER_ANIM_TEXT_BACKGROUND = 0x75000000;

    private static final int ANIM_DURATION = 500;

    public SlingImageView(Context context) {
        super(context);
    }

    public SlingImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SlingImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SlingImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
        setAlpha(0f);
        setImageBitmap(bitmap);

        final Interpolator interpolator = new AccelerateDecelerateInterpolator();
        startBitmapAnimation(interpolator);
        final TextView text = (TextView) getTag();
        if (null != text) {
            startTextAnimation(interpolator, text);
        }
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable) {
        setImageDrawable(errorDrawable);
    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable) {
        setImageDrawable(placeHolderDrawable);
    }

    private void startBitmapAnimation(Interpolator interpolator) {
        final PropertyValuesHolder alpha =
                PropertyValuesHolder.ofFloat(View.ALPHA, 0f, 1f);
        final PropertyValuesHolder scaleX =
                PropertyValuesHolder.ofFloat(View.SCALE_X, .9f, 1.1f, 1f);
        final PropertyValuesHolder scaleY =
                PropertyValuesHolder.ofFloat(View.SCALE_Y, .9f, 1.1f, 1f);

        final ObjectAnimator animator =
                ObjectAnimator.ofPropertyValuesHolder(this, alpha, scaleX, scaleY);
        animator.setDuration(ANIM_DURATION);
        animator.setInterpolator(interpolator);
        animator.start();
    }

    private void startTextAnimation(Interpolator interpolator, TextView text) {
        final ArgbEvaluator evaluator = new ArgbEvaluator();

        final PropertyValuesHolder textColor =
                PropertyValuesHolder.ofObject("textColor", evaluator,
                        BEFORE_ANIM_TEXT_COLOR, AFTER_ANIM_TEXT_COLOR);
        final PropertyValuesHolder backgroundColor =
                PropertyValuesHolder.ofObject("backgroundColor", evaluator,
                        BEFORE_ANIM_TEXT_BACKGROUND, AFTER_ANIM_TEXT_BACKGROUND);

        final ObjectAnimator anim =
                ObjectAnimator.ofPropertyValuesHolder(text, textColor, backgroundColor);
        anim.setDuration(ANIM_DURATION);
        anim.setInterpolator(interpolator);
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                setTag(null); // Release the TextView
            }
        });
        anim.start();
    }

}
