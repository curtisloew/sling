Sling
===

Sling is a simple demo app for the nice people at Fling. The app works on API 14+ and should look reasonably good on tablets too.

It was conceived in two days and as such, it's a bit crude. One important missing feature are unit tests, which were left out due to time constraints.

As for the code, a few conscious decisions were made:

 - image placeholders are missing, because I think it actually looks nicer without them.
 -  `RecyclerView` is not used to show you I also know how to efficiently work with _older_ stuff.

Third Party Libs
---

- Otto
- Gson
- Retrofit
- Picasso
- OrmLite
- Design Support Package
